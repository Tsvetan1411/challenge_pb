import easygui as eg
from PyPDF2 import PdfReader, PdfWriter

source_path = eg.fileopenbox(
    title="Chose file...",
    default="*.pdf",
)

if source_path == None:
    exit()

input_file = PdfReader(source_path)
num_of_pages = len(input_file.pages)

start_page = eg.enterbox(
    msg="Enter start page"
)

if start_page == None:
    exit()

while (
    not start_page.isdigit() 
    or int(start_page) not in range(num_of_pages + 1)
    or int(start_page) == 0
):
    eg.msgbox(msg="Page not found")
    start_page = eg.enterbox(
    msg="Enter start page"
    )
    if start_page == None:
        exit()

end_page = eg.enterbox(
    msg="Enter end page"
)

if end_page == None:
    exit()

while (
    not end_page.isdigit()
    or int(end_page) not in range(num_of_pages + 1)
    or int(end_page) < int(start_page)
):
    eg.msgbox(msg="Page not found")
    end_page = eg.enterbox(
    msg="Enter end page"
    )
    if end_page == None:
        exit()

output_path = eg.filesavebox(title="Where to save?")

if output_path == None:
    exit()

while input_file == output_path:
    eg.msgbox(msg="Cannot overwrite!")
    output_path = eg.filesavebox(title="Where to save?")
    if output_path == None:
        exit()

pdf_writer = PdfWriter()

for page_num in range((int(start_page)) - 1, int(end_page)):
    page = input_file.pages[page_num]
    pdf_writer.add_page(page)

with open(output_path, "wb") as output_file:
    pdf_writer.write(output_file)