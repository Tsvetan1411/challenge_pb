from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

def get_text(page):
    return page.extract_text()

pdf_path = Path("C:/code/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/scrambled.pdf")

pdf_reader = PdfReader(str(pdf_path))
pdf_writer = PdfWriter()

pages = list(pdf_reader.pages)
pages.sort(key=get_text)

for page in pages:
    rotation = page["/Rotate"]
    page.rotate(-rotation)
    pdf_writer.add_page(page)

output_path = Path.home() / "unscrambled.pdf"
with output_path.open(mode="wb") as file:
    pdf_writer.write(file)