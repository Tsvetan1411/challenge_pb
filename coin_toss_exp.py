import random


def flip_coin_trial():
    """Simulates coin toss trials until both heads and tails occur,
        tracking the number of attempts needed."""
    tail_tally = 0
    heads_tally = 0
    number_of_flips = 0
    while tail_tally == 0 or heads_tally == 0:
        if random.randint(0, 1) == 0:
            tail_tally += 1
        else:
            heads_tally += 1
        number_of_flips += 1
    return number_of_flips


number_of_attemps_total = 0

for sim_run in range(10000):
    number_of_attemps_total += flip_coin_trial()

print(f"The averege numbers of flips = {number_of_attemps_total / 10000}")
