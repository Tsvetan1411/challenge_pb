class Animal:
    def __init__(self, name, weight, age, stamina):
        self.name = name
        self.weight = weight
        self.age = age
        self.stamina = stamina

    def run(self):
        self.stamina -= 2

    def walk(self):
        self.stamina -= 1

    def eat(self, amount_of_food):
        if self.stamina == 100:
            print("I am not hungry!")
        elif 100 - self.stamina < amount_of_food:
            print("I am not that hungry!")
        else:
            self.stamina += amount_of_food

    def introduce(self):
        print(f"My name is {self.name}. My age is {self.age}. My weight is {self.weight} kg.")


class Bird:
    def fly(self):
        self.stamina -= 5

    def lay_egg(self):
        self.stamina -= 10


class Chicken(Animal, Bird):
    def __init__(self, rasa, name, weight, age, stamina):
        self.rasa = rasa
        super().__init__(name, weight, age, stamina)


class Dog(Animal):
    def __init__(self, sound, name, weight, age, stamina):
        self.sound = sound
        super().__init__(name, weight, age, stamina)

    def make_sound(self):
        print(f"{self.sound} " * 2)


class Cow(Animal):
    def give_milk(self):
        if self.stamina <= 90:
            print("Feed me first!")
        else:
            print(f"Cow gave You {self.stamina - 90} L of milk")


class Cat(Animal):
    def eat(self, amount_of_food):
        self.stamina += amount_of_food
        print("Give me more!!!")
