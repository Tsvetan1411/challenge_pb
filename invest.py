def invest(amount, rate, years):
    """
    Calculates the actual amount for each year based on the given parameters.

    Args:
        amount (float): The initial principal amount.
        rate (float): The annual interest rate as a decimal.
        years (int): The number of years for which to calculate the actual amounts.
    """
    for x in range(1, years + 1):
        amount = amount*(rate + 1)
        print(f"year {x}: ${amount:.2f}")


amount = float(input("Enter a initial amount: "))
rate = float(input("Enter an annual precentage rate: "))
years = int(input("Enter a number of years for investytion: "))

invest(amount, rate, years)
