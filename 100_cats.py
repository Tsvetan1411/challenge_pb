# Initialize a dictionary to represent cats and number of stops on every cat
cats = {}

# Initialize each cat with no stops
for x in range(1, 101):
    cats[x] = 0

# Counting number of stops on every cat
for cat in range(1, 101):
    for round in range(1, 101):
        if (cat % round) == 0:
            cats[cat] += 1

# If the number of stops on cat is odd, it has a hat
for number_of_cat in cats:
    if (cats[number_of_cat] % 2) != 0:
        print(f"The cat in position {number_of_cat} is wearing the hat!")
