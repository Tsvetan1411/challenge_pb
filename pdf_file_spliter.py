from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

class PdfFileSpliter:
    writer1 = PdfWriter()
    writer2 = PdfWriter()
    def __init__(self, file_name):
        self.file_name = file_name
        self.pdf_reader = PdfReader(f"C:/code/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/{file_name}")

    def split(self, breakpoint):
        for page in self.pdf_reader.pages[:breakpoint]:
            self.writer1.add_page(page)
        for page in self.pdf_reader.pages[breakpoint:]:
            self.writer2.add_page(page)

    def write(self, filename):
        path1 = Path(f"C:/code/{filename}_1.pdf")
        path2 = Path(f"C:/code/{filename}_2.pdf")
        with path1.open(mode="wb") as file:
            self.writer1.write(file)
        with path2.open(mode="wb") as file:
            self.writer2.write(file)

pride_justice = PdfFileSpliter("Pride_and_Prejudice.pdf")
pride_justice.split(150)
pride_justice.write("pd_split")