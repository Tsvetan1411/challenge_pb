def convert_cel_to_far(cel):
    """Take input in degrees Celsius and return value converted to degrees Fahrenheit"""
    to_fahrenheit = (cel*9/5) + 32
    return to_fahrenheit


def convert_far_to_cel(far):
    """Take input in degrees Fahrenheit and return value converted to degrees Celsius"""
    to_celsius = (far - 32)*5/9
    return to_celsius


user_input_cel = float(input("Enter a temperature in degrees C: "))
print(f"{user_input_cel} degrees C = {convert_cel_to_far(user_input_cel):.2f} degrees F")

user_input_far = float(input("Enter a temperature in degrees F: "))
print(f"{user_input_far} degrees F = {convert_far_to_cel(user_input_far):.2f} degrees C")
