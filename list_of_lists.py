from statistics import median, mean


univer_enrollment = []
univer_tuition_fees = []


def enrolmment_stats(list_of_lists):
    for row in list_of_lists:
        univer_enrollment.append(row[1])
        univer_tuition_fees.append(row[2])


universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

enrolmment_stats(universities)

print("*****" * 5)
print(f"Total students: {sum(univer_enrollment):,}")
print(f"Total tuition: $ {sum(univer_tuition_fees):,}")
print(f"\nStudent mean: {mean(univer_enrollment):,.2f}")
print(f"Student median: {median(univer_enrollment):,}")
print(f"\nTuition mean: $ {mean(univer_tuition_fees):,.2f}")
print(f"Tuition median: $ {median(univer_tuition_fees):,}")
print("*****" * 5)
