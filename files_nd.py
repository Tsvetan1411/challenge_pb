import pathlib

images = pathlib.Path.home() / "practice_files" / "images"
images.mkdir(exist_ok=True)

practice_files = pathlib.Path.home() / "practice_files"

for path in practice_files.rglob("*.*"):
    if path.suffix in [".png", ".gif", ".jpg"]:
        path.replace(images / path.name)
