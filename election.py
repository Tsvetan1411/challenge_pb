import random

def election_result(chance_of_winning):
    if random.random() < chance_of_winning:
        return 1
    else:
        return 0
    
chance_A_in_reg_1 = 0.87
chance_A_in_reg_2 = 0.65
chance_A_in_reg_3 = 0.17
summary_wins_A = 0

for trial in range(10000):
    res_in_1 = election_result(chance_A_in_reg_1)
    res_in_2 = election_result(chance_A_in_reg_2)
    res_in_3 = election_result(chance_A_in_reg_3)
    if (res_in_1 + res_in_2 + res_in_3) >= 2:
        summary_wins_A += 1

percent_of_wins = summary_wins_A / 10000
print(f"Candidate A wins in {percent_of_wins:.2%} trials")