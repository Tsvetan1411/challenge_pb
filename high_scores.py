from pathlib import Path
import csv
 

path = Path.home() / "scores.csv"
with path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    scores_list = [row for row in reader]

high_scores_dic = {}
for record in scores_list:
    name = record["name"]
    score = int(record["score"])
    if name not in high_scores_dic:
        high_scores_dic[name] = score
    else:
        if score > high_scores_dic[name]:
            high_scores_dic[name] = score

path_high_scores = Path.home() / "high_scores.csv"
with path_high_scores.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for row in high_scores_dic:
        row_dic = {"name": row, "high_score": high_scores_dic[row]}
        writer.writerow(row_dic)
