from random import choice

number_of_n_v_a = 3
number_of_prepositions = 2
number_of_adverb = 1
vovels = "aeiou"

def random_words(words_list, word_type):
    list_of_words = []
    for number in range(0, word_type):
        list_of_words.append(choice(words_list))
    return list_of_words

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

list_of_nouns = random_words(nouns, number_of_n_v_a)
list_of_verbs = random_words(verbs, number_of_n_v_a)
list_of_adjevtives = random_words(adjectives, number_of_n_v_a)
list_of_prepositions = random_words(prepositions, number_of_prepositions)
list_of_adverb = random_words(adverbs, number_of_adverb)

print(f"\nA {list_of_adjevtives[0]} {list_of_nouns[0]}")
print(f"\nA {list_of_adjevtives[0]} {list_of_nouns[0]} {list_of_verbs[0]} {list_of_prepositions[0]} the {list_of_adjevtives[1]} {list_of_nouns[1]}")
print(f"{list_of_adverb[0]}, the {list_of_nouns[0]} {list_of_verbs[1]}")
print(f"the {list_of_nouns[1]} {list_of_verbs[2]} {list_of_prepositions[1]} a {list_of_adjevtives[2]} {list_of_nouns[2]}")
print(f"\n")
